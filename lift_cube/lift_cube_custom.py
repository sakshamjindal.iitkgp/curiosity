# Custom environment with instrinsic reward

from collections import OrderedDict
from pathlib import Path
from typing import Dict, List, Union

import numpy as np
import sapien.core as sapien
import trimesh
from sapien.core import Pose
from scipy.spatial.distance import cdist
from transforms3d.euler import euler2quat


from mani_skill2.envs.pick_and_place.pick_cube import LiftCubeEnv
from mani_skill2.utils.registration import register_env
import numpy as np
import sapien.core as sapien
import torch

@register_env("LiftCube-v3", max_episode_steps=200)
class LiftCubeCuriosityEnv(LiftCubeEnv):
    def __init__(self, curiosity_type, *args, curiosity_model = None, **kwargs):
        self.curiosity_type = curiosity_type
        if curiosity_type != "":
            print(f"Using {curiosity_type} for curiosity")
        else:
            print("Non-curious agent")
        if curiosity_model is None:
            print("No curiosity model is loaded")
        else:
            print("Curiosity model is loaded")

        if curiosity_type == "RND":
            self.RND_model = curiosity_model
        super().__init__(*args, **kwargs)
    
    
    def compute_dense_reward(self, info, **kwargs):
        reward = self.get_extrinsic_reward(info)
        if reward is None:
            reward = 0 
        reward += 0.3 * self.get_intrinsic_reward(info)
        return reward
    
    
    def get_intrinsic_reward(self, info):
        if self.curiosity_type == "RND":
            curr_obs = self.get_obs()
            curr_obs = torch.from_numpy(curr_obs).float().to(self.RND_model.device)
            target_feature = self.RND_model.target(curr_obs)
            predict_feature = self.RND_model.predictor(curr_obs)
            intrinsic_reward = torch.linalg.norm(target_feature.flatten() - predict_feature.flatten())
            intrinsic_reward = intrinsic_reward.cpu().detach().numpy()
            return intrinsic_reward
        else:
            return 0
        
        
    def get_extrinsic_reward(self, info):
        reward = 0.0

        if info["success"]:
            reward += 2.25
            return rewards

        # reaching reward
        gripper_pos = self.tcp.get_pose().p
        obj_pos = self.obj.get_pose().p
        dist = np.linalg.norm(gripper_pos - obj_pos)
        reaching_reward = 1 - np.tanh(5 * dist)
        reward += reaching_reward

        is_grasped = self.agent.check_grasp(self.obj, max_angle=30)

        # grasp reward
        if is_grasped:
            reward += 0.25

        # lifting reward
        if is_grasped:
            lifting_reward = self.obj.pose.p[2] - self.cube_half_size[2]
            lifting_reward = min(lifting_reward / self.goal_height, 1.0)
            reward += lifting_reward

        return reward