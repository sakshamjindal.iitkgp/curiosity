# Custom environment with instrinsic reward

from collections import OrderedDict
from pathlib import Path
from typing import Dict, List, Union

import numpy as np
import sapien.core as sapien
import trimesh
from sapien.core import Pose
from scipy.spatial.distance import cdist
from transforms3d.euler import euler2quat


from mani_skill2.envs.misc.turn_faucet import TurnFaucetEnvSubset
from mani_skill2.utils.registration import register_env
import numpy as np
import sapien.core as sapien
import torch

@register_env("TurnFaucet-v4", max_episode_steps=200)
class TurnFaucetCuriosityEnv(TurnFaucetEnvSubset):
    def __init__(self, curiosity_type, *args, curiosity_model = None, **kwargs):
        self.curiosity_type = curiosity_type
        if curiosity_type != "":
            print(f"Using {curiosity_type} for curiosity")
        else:
            print("Non-curious agent")
        if curiosity_model is None:
            print("No curiosity model is loaded")
        else:
            print("Curiosity model is loaded")

        if curiosity_type == "RND":
            self.RND_model = curiosity_model
        super().__init__(*args, **kwargs)
    
    
    def compute_dense_reward(self, info, **kwargs):
        reward = self.get_extrinsic_reward(info)
        if reward is None:
            reward = 0 
        reward += 0.3 * self.get_intrinsic_reward(info)
        return reward
    
    
    def get_intrinsic_reward(self, info):
        if self.curiosity_type == "RND":
            curr_obs = self.get_obs()
            curr_obs = torch.from_numpy(curr_obs).float().to(self.RND_model.device)
            target_feature = self.RND_model.target(curr_obs)
            predict_feature = self.RND_model.predictor(curr_obs)
            intrinsic_reward = torch.linalg.norm(target_feature.flatten() - predict_feature.flatten())
            intrinsic_reward = intrinsic_reward.cpu().detach().numpy()
            return intrinsic_reward
        else:
            return 0
        
        
    def get_extrinsic_reward(self, info):
        reward = 0.0

        if info["success"]:
            return 10.0

        distance = self._compute_distance()
        reward += 1 - np.tanh(distance * 5.0)

        # is_contacted = any(self.agent.check_contact_fingers(self.target_link))
        # if is_contacted:
        #     reward += 0.25

        angle_diff = self.target_angle - self.current_angle
        turn_reward_1 = 3 * (1 - np.tanh(max(angle_diff, 0) * 2.0))
        reward += turn_reward_1

        delta_angle = angle_diff - self.last_angle_diff
        if angle_diff > 0:
            turn_reward_2 = -np.tanh(delta_angle * 2)
        else:
            turn_reward_2 = np.tanh(delta_angle * 2)
        turn_reward_2 *= 5
        reward += turn_reward_2
        self.last_angle_diff = angle_diff

        return reward


@register_env("TurnFaucet-v3", max_episode_steps=200)
class TurnFaucetCustomRewardEnv(TurnFaucetEnvSubset):
    def compute_dense_reward(self, info, **kwargs):
        reward = 0.0

        if info["success"]:
            return 10.0

        distance = self._compute_distance()
        reward += 1 - np.tanh(distance * 5.0)

        # is_contacted = any(self.agent.check_contact_fingers(self.target_link))
        # if is_contacted:
        #     reward += 0.25

        angle_diff = self.target_angle - self.current_angle
        turn_reward_1 = 3 * (1 - np.tanh(max(angle_diff, 0) * 2.0))
        reward += turn_reward_1

        delta_angle = angle_diff - self.last_angle_diff
        if angle_diff > 0:
            turn_reward_2 = -np.tanh(delta_angle * 2)
        else:
            turn_reward_2 = np.tanh(delta_angle * 2)
        turn_reward_2 *= 5
        reward += turn_reward_2
        self.last_angle_diff = angle_diff

        return reward